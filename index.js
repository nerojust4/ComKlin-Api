// Import required modules
const express = require('express');
const bodyParser = require('body-parser');
const swaggerJsdoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const swaggerOptions = require('./swaggerOptions');
const path = require('path');
const http = require('http'); // Import http module for creating server
const SocketUtil = require('./src/utils/SocketUtil');

const { sequelize } = require('./src/models');
const userRoute = require('./src/routes/UserRouter');
const authRoute = require('./src/routes/AuthRouter');
const registerRoute = require('./src/routes/RegisterRouter');
const roleRouter = require('./src/routes/RoleRouter');
const eventRouter = require('./src/routes/EventRouter');
const postRouter = require('./src/routes/PostRouter');
const badgeRouter = require('./src/routes/BadgeRouter');

// Create an instance of Express
const app = express();

// Middleware to parse JSON bodies
app.use(bodyParser.json());

// Set up Swagger documentation
const specs = swaggerJsdoc(swaggerOptions);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(specs));

// Serve uploaded files statically
app.use('/src/uploads', express.static(path.join(__dirname, 'src/uploads')));

// Initialize socket.io
const server = http.createServer(app);
SocketUtil.initialize(server);

// User routes
app.use(userRoute);
app.use(authRoute);
app.use(registerRoute);
app.use(roleRouter);
app.use(eventRouter);
app.use(postRouter);
app.use(badgeRouter);

// Landing route
app.get('/', (_req, res) => {
  const welcomeMessage = `Welcome to HCMC Environmental Cleanup API. Access our <a href='${process.env.NGROK_HOST}/api-docs'>api-docs here</a>.`;
  res.send(welcomeMessage);
});

// Sync Sequelize models with the database
sequelize
  .sync({ force: false, alter: false })
  .then(() => {
    console.log('Database synchronized successfully');
  })
  .catch((err) => {
    console.error('Unable to sync database:', err);
  });

// Start the server
const PORT = process.env.PORT || 9000;
server.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
