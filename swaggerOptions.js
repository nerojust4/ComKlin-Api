const dotenv = require('dotenv');
const apiList = require('./src/utils/apiList.json'); // Load apiList from the routes folder

// Load environment variables from .env file
dotenv.config();

// Construct base URL using PORT environment variable or default to 3000
const PORT = process.env.PORT || 3000;
const baseUrl = `http://localhost:${PORT}`;
const ngrokBaseUrl = process.env.NGROK_HOST;

// Define Swagger options
const swaggerOptions = {
  swaggerDefinition: {
    openapi: '3.0.0',
    info: {
      title: 'HCMC ENVIRONMENTAL CLEANUP API',
      version: '1.0.0',
      description:
        'API Documentation for HCMC Ennvironmental System'
    },
    servers: [
      {
        url: ngrokBaseUrl,
        description: 'NGROK server'
      },
      {
        url: baseUrl,
        description: 'Development server'
      }
    ],
    security: [
      {
        BearerAuth: []
      }
    ]
  },
  apis: apiList
};

module.exports = swaggerOptions;
