# Stewardship and Pollution API Documentation

This API provides endpoints to manage stewardship activities and access pollution data related to water and air.

## Getting Started

To get started with the API, follow these steps:

1. **Installation**

   Clone the repository:
   ```
   git clone https://gitlab.com/nerojust4/ComKlin-Api
   ```
   Install dependencies:
   ```
   npm install
   ```

2. **Running the API**

   Start the server:
   ```
   npm start
   ```
   The API will run on `http://localhost:8500` by default.

3. **Folder Structure**

   ```
   controllers/
   ├── AuthController.js       // Handles authentication logic
   ├── BadgeController.js      // Manages badge operations
   ├── EventController.js      // Processes event actions
   ├── FeedController.js       // Manages feed operations
   ├── PostController.js       // Handles post functionalities
   ├── ResourceController.js   // Controls resource management
   ├── RoleController.js       // Manages role operations
   └── UserController.js       // Handles user logic

   middleware/
   ├── AuthMiddleware.js       // Authentication middleware
   ├── PermissionMiddleware.js // Permission validation middleware
   └── ...                     // Other middleware files

   models/
   ├── Administrator.js        // Model for administrators
   ├── AdminUser.js            // Model for admin users
   ├── Analytics.js            // Model for analytics data
   ├── AppConfig.js            // Model for application configuration
   ├── Associations.js         // Model for associations
   ├── AuditLog.js             // Model for audit logs
   ├── Badge.js                // Model for badges
   ├── BlacklistedToken.js     // Model for blacklisted tokens
   ├── Event.js                // Model for events
   ├── EventMedia.js           // Model for event media
   ├── EventUser.js            // Model for event participants
   ├── Feature.js              // Model for features
   ├── Geolocation.js          // Model for geolocation data
   ├── Message.js              // Model for messages
   ├── MessageReply.js         // Model for message replies
   ├── Permission.js           // Model for permissions
   ├── PollutionReport.js      // Model for pollution reports
   ├── Post.js                 // Model for posts
   ├── PostMedia.js            // Model for post media
   ├── PushNotification.js     // Model for push notifications
   ├── Resource.js             // Model for resources
   ├── Role.js                 // Model for roles
   ├── RoleFeature.js          // Model for role features
   ├── RolePermission.js       // Model for role permissions
   ├── Session.js              // Model for user sessions
   ├── User.js                 // Model for users
   ├── UserRole.js             // Model for user roles
   └── Volunteer.js            // Model for volunteers

   routes/
   ├── AuthRouter.js           // Routes for authentication
   ├── BadgeRouter.js          // Routes for managing badges
   ├── EventRouter.js          // Routes for events
   ├── FeedRouter.js           // Routes for managing feeds
   ├── PostRouter.js           // Routes for handling posts
   ├── ResourceRouter.js       // Routes for managing resources
   ├── RoleRouter.js           // Routes for managing roles
   └── UserRouter.js           // Routes for user actions

   uploads/
   ├── ...                     // Uploads folder (if applicable)

   utils/
   ├── ...                     // Utility functions used throughout the project
   ```

4. **Data Format**

   - Events Response Example:
     ```json
     {
       "status": true,
       "result": {
         "events": [
           {
             "id": 1,
             "title": "Hackathon HCMC",
             "description": "Event of the year.",
             "date": "2024-06-29T00:12:44.000Z",
             "location": "HCMC, District 7, Tiny Flower Montessori"
           },
           {
             "id": 2,
             "title": "Hackathon Singapore",
             "description": "Event of the year.",
             "date": "2024-06-29T00:12:44.000Z",
             "location": "HCMC, District 7, BCIS"
           },
           {
             "id": 3,
             "title": "End of Month Cleanup",
             "description": "End of the month cleaning at my location.",
             "date": "2024-06-29T15:34:02.000Z",
             "location": "District 7, CIS"
           }
         ],
         "totalPages": 2
       },
       "errors": []
     }
     ```

5. **Authentication**

   This API requires authentication for accessing certain endpoints.

6. **Contributing**

   Contributions are welcome! Please fork the repository and create a pull request with your changes.

7. **License**

   This project is licensed under the MIT License - see the [LICENSE](./LICENSE) file for details.

8. **Contact**

   For questions or support, contact [Adjekughene Nyerhovwo](mailto:nerojust4@gmail.com).
