const { User, Permission, RolePermission } = require('../models');
const ApiResponse = require('../utils/ApiResponse');

const validatePermissionAccessOfRequester = async (
  req,
  res,
  next,
  permissionName
) => {
  try {
    console.log('Checking user permission', req.user.userId);
    const user = await User.findByPk(req.user.userId);
    if (!user) {
      return ApiResponse.error(res, 500, 'User not found');
    }
    // console.log('user is ', user);
    const permission = await Permission.findOne({
      where: { name: permissionName }
    });
    if (!permission) {
      return ApiResponse.error(res, 500, 'Permission not found');
    }
    console.log('Found the permission');
    const rolePermission = await RolePermission.findOne({
      where: { roleId: user.roleId, permissionId: permission.id }
    });
    if (!rolePermission) {
      console.log('Permission denied');
      return ApiResponse.error(res, 403, 'Permission denied, contact admin');
    }
    console.log('Permission granted for', req.user.userId);

    next();
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};

const validatePermission = (permissionName) => {
  return (req, res, next) => {
    validatePermissionAccessOfRequester(req, res, next, permissionName);
  };
};

module.exports = { validatePermission };
