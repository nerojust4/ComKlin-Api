const jwt = require("jsonwebtoken");
const { User, BlacklistedToken } = require("../models");
const ApiResponse = require("../utils/ApiResponse");
const {
  INVALID_TOKEN,
  TOKEN_REQUIRED,
  BLACKLISTED_OR_EXPIRED_TOKEN,
  USER_NOT_FOUND,
} = require("../utils/Strings");

const authenticateToken = async (req, res, next) => {
  // console.log("token",req.headers["authorization"])
  const token =
    req.headers["authorization"]?.split(" ")[0] === "Bearer"
      ? req.headers["authorization"]?.split(" ")[1]
      : req.headers["authorization"];

  if (!token) {
    return ApiResponse.error(res, 401, TOKEN_REQUIRED);
  }

  try {
    const blacklistedToken = await BlacklistedToken.findOne({
      where: { token },
    });

    if (blacklistedToken) {
      return ApiResponse.error(res, 401, BLACKLISTED_OR_EXPIRED_TOKEN);
    }

    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    req.user = decoded;

    const user = await User.findByPk(decoded.userId);
    if (!user) {
      return ApiResponse.error(res, 401, USER_NOT_FOUND);
    }

    next();
  } catch (error) {
    console.log(INVALID_TOKEN)
    return ApiResponse.error(res, 401, INVALID_TOKEN);
  }
};

module.exports = { authenticateToken };
