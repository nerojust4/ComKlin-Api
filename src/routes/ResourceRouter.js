const express = require('express');
const router = express.Router();
const PollutionResourceController = require('../controllers/PollutionResourceController');
const { authenticateToken } = require('../middleware/AuthMiddleware');

/**
 * @swagger
 * /api/v1/resource:
 *   post:
 *     summary: Create a new pollution resource
 *     description: Create a new pollution resource with the provided details
 *     tags:
 *       - Resources
 *     security:
 *       - BearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               title:
 *                 type: string
 *               description:
 *                 type: string
 *               link:
 *                 type: string
 *               category:
 *                 type: string
 *               image:
 *                 type: string
 *     responses:
 *       '201':
 *         description: Created
 *       '400':
 *         description: Bad request
 */
router.post(
  '/api/v1/resource',
  // authenticateToken,
  PollutionResourceController.createPollutionResource
);

/**
 * @swagger
 * /api/v1/resource:
 *   get:
 *     summary: Get all pollution resources
 *     description: Retrieve a list of all pollution resources
 *     tags:
 *       - Resources
 *     parameters:
 *       - in: query
 *         name: page
 *         description: The page number to retrieve
 *         schema:
 *           type: integer
 *           default: 1
 *       - in: query
 *         name: pageSize
 *         description: The number of resources per page
 *         schema:
 *           type: integer
 *           default: 10
 *     security:
 *       - BearerAuth: []
 *     responses:
 *       '200':
 *         description: OK
 *       '401':
 *         description: Unauthorized
 */
router.get(
  '/api/v1/resource',
  // authenticateToken,
  PollutionResourceController.getAllPollutionResources
);

/**
 * @swagger
 * /api/v1/resource/{id}:
 *   get:
 *     summary: Get pollution resource by ID
 *     description: Retrieve a pollution resource by its ID
 *     tags:
 *       - Resources
 *     security:
 *       - BearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         description: ID of the pollution resource to retrieve
 *         required: true
 *         schema:
 *           type: integer
 *     responses:
 *       '200':
 *         description: OK
 *       '401':
 *         description: Unauthorized
 *       '404':
 *         description: Not found
 */
router.get(
  '/api/v1/resource/:id',
  // authenticateToken,
  PollutionResourceController.getPollutionResourceById
);

/**
 * @swagger
 * /api/v1/resource/{id}:
 *   put:
 *     summary: Update pollution resource by ID
 *     description: Update a pollution resource by its ID
 *     tags:
 *       - Resources
 *     security:
 *       - BearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         description: ID of the pollution resource to update
 *         required: true
 *         schema:
 *           type: integer
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               title:
 *                 type: string
 *               description:
 *                 type: string
 *               link:
 *                 type: string
 *               category:
 *                 type: string
 *               image:
 *                 type: string
 *     responses:
 *       '200':
 *         description: OK
 *       '401':
 *         description: Unauthorized
 *       '404':
 *         description: Not found
 */
router.put(
  '/api/v1/resource/:id',
  authenticateToken,
  PollutionResourceController.updatePollutionResource
);

/**
 * @swagger
 * /api/v1/resource/{id}:
 *   delete:
 *     summary: Delete pollution resource by ID
 *     description: Delete a pollution resource by its ID
 *     tags:
 *       - Resources
 *     security:
 *       - BearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         description: ID of the pollution resource to delete
 *         required: true
 *         schema:
 *           type: integer
 *     responses:
 *       '204':
 *         description: No content
 *       '401':
 *         description: Unauthorized
 *       '404':
 *         description: Not found
 */
router.delete(
  '/api/v1/resource/:id',
  authenticateToken,
  PollutionResourceController.deletePollutionResource
);

module.exports = router;
