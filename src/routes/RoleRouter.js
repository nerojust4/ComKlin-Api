const express = require("express");
const router = express.Router();
const RoleController = require("../controllers/RoleController");
const { authenticateToken } = require("../middleware/AuthMiddleware");

/**
 * @swagger
 * /api/v1/role:
 *   post:
 *     summary: Create a new role
 *     description: Create a new role with the provided details
 *     tags:
 *       - Roles
 *     security:
 *       - BearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *     responses:
 *       '201':
 *         description: Created
 *       '400':
 *         description: Bad request
 */
router.post("/api/v1/role",
// authenticateToken,
 RoleController.createRole);

/**
 * @swagger
 * /api/v1/role:
 *   get:
 *     summary: Get all roles
 *     description: Retrieve a list of all roles
 *     tags:
 *       - Roles
 *     parameters:
 *       - in: query
 *         name: page
 *         description: The page number to retrieve
 *         schema:
 *           type: integer
 *           default: 1
 *       - in: query
 *         name: pageSize
 *         description: The number of users per page
 *         schema:
 *           type: integer
 *           default: 10
 *     security:
 *       - BearerAuth: []
 *     responses:
 *       '200':
 *         description: OK
 *       '401':
 *         description: Unauthorized
 */
router.get("/api/v1/role",
//  authenticateToken,
  RoleController.getAllRoles);

/**
 * @swagger
 * /api/v1/role/{id}:
 *   get:
 *     summary: Get role by ID
 *     description: Retrieve a role by its ID
 *     tags:
 *       - Roles
 *     security:
 *       - BearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         description: ID of the role to retrieve
 *         required: true
 *         schema:
 *           type: integer
 *     responses:
 *       '200':
 *         description: OK
 *       '401':
 *         description: Unauthorized
 *       '404':
 *         description: Not found
 */
router.get("/api/v1/role/:id", authenticateToken, RoleController.getRoleById);

/**
 * @swagger
 * /api/v1/role/{id}:
 *   put:
 *     summary: Update role by ID
 *     description: Update a role by its ID
 *     tags:
 *       - Roles
 *     security:
 *       - BearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         description: ID of the role to update
 *         required: true
 *         schema:
 *           type: integer
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *     responses:
 *       '200':
 *         description: OK
 *       '401':
 *         description: Unauthorized
 *       '404':
 *         description: Not found
 */
router.put("/api/v1/role/:id", 
// authenticateToken, 
RoleController.updateRole);

/**
 * @swagger
 * /api/v1/role/{id}:
 *   delete:
 *     summary: Delete role by ID
 *     description: Delete a role by its ID
 *     tags:
 *       - Roles
 *     security:
 *       - BearerAuth: []
 *     parameters:
 *       - in: query
 *         name: roleId
 *         description: ID of the role to delete
 *         required: true
 *         schema:
 *           type: integer
 *       - in: query
 *         name: userId
 *         description: ID of the user attempting to delete the lecturer
 *         required: true
 *         schema:
 *           type: integer
 *     responses:
 *       '204':
 *         description: No content
 *       '401':
 *         description: Unauthorized
 *       '404':
 *         description: Not found
 */
router.delete("/api/v1/role/:id", authenticateToken, RoleController.deleteRole);

module.exports = router;
