const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserController');
const { authenticateToken } = require('../middleware/AuthMiddleware');
const { validatePermission } = require('../middleware/PermissionMiddleware');

/**
 * @swagger
 * components:
 *   securitySchemes:
 *     BearerAuth:
 *       type: http
 *       scheme: bearer
 *       bearerFormat: JWT
 *   schemas:
 *     CreateUserRequest:
 *       type: object
 *       properties:
 *         firstName:
 *           type: string
 *           minLength: 1
 *           maxLength: 50
 *         lastName:
 *           type: string
 *           minLength: 1
 *           maxLength: 50
 *         username:
 *           type: string
 *           minLength: 3
 *           maxLength: 30
 *         email:
 *           type: string
 *           format: email
 *         phoneNumber:
 *           type: string
 *         gender:
 *           type: string
 *         state:
 *           type: string
 *         country:
 *           type: string
 *         password:
 *           type: string
 *           minLength: 6
 *           maxLength: 30
 *         imageUrl:
 *           type: string
 *         roleId:
 *           type: integer
 *           default: 3
 *       required:
 *         - firstName
 *         - lastName
 *         - username
 *         - email
 *         - phoneNumber
 *         - gender
 *         - city
 *         - state
 *         - country
 *         - password
 *         - roleId
 */

/**
 * @swagger
 * /api/v1/user:
 *   post:
 *     summary: Create a new user
 *     description: Create a new user with the provided details
 *     tags:
 *       - Users
 *     security:
 *       - BearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/CreateUserRequest'
 *     responses:
 *       '201':
 *         description: Created
 *       '400':
 *         description: Bad request
 */
router.post(
  '/api/v1/user',
  // validatePermission('CREATE_USER'),
  UserController.createUser
);

/**
 * @swagger
 * /api/v1/user:
 *   get:
 *     summary: Get users with pagination and search
 *     description: Retrieve a paginated list of users with optional search functionality.
 *     tags:
 *       - Users
 *     security:
 *       - BearerAuth: []
 *     parameters:
 *       - in: query
 *         name: page
 *         description: The page number to retrieve
 *         schema:
 *           type: integer
 *           default: 1
 *       - in: query
 *         name: pageSize
 *         description: The number of users per page
 *         schema:
 *           type: integer
 *           default: 10
 *       - in: query
 *         name: keywords
 *         description: Keywords to search for in users records
 *         schema:
 *           type: string
 *       - in: query
 *         name: all
 *         description: Fetch all users without pagination if set to true
 *         schema:
 *           type: boolean
 *           default: false
 *     responses:
 *       '200':
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 users:
 *                   type: array
 *                   items:
 *                 totalPages:
 *                   type: integer
 *       '401':
 *         description: Unauthorized
 */

router.get(
  '/api/v1/user',
  // authenticateToken,
  // validatePermission('GET_USER'),
  UserController.getAllUsers
);

/**
 * @swagger
 * /api/v1/user/{id}:
 *   get:
 *     summary: Get user by ID
 *     description: Retrieve a user by their ID
 *     tags:
 *       - Users
 *     security:
 *       - BearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         description: ID of the user to retrieve
 *         required: true
 *         schema:
 *           type: integer
 *     responses:
 *       '200':
 *         description: OK
 *       '401':
 *         description: Unauthorized
 *       '404':
 *         description: Not found
 */
router.get(
  '/api/v1/user/:id',
  authenticateToken,
  // validatePermission('GET_USER'),
  UserController.getUserById
);

/**
 * @swagger
 * /api/v1/user/{id}:
 *   put:
 *     summary: Update user by ID
 *     description: Update a user by their ID with the provided details
 *     tags:
 *       - Users
 *     security:
 *       - BearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         description: ID of the user to update
 *         required: true
 *         schema:
 *           type: integer
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               phoneNumber:
 *                 type: string
 *               email:
 *                 type: string
 *     responses:
 *       '200':
 *         description: OK
 *       '400':
 *         description: Bad request
 *       '404':
 *         description: Not found
 */
router.put(
  '/api/v1/user/:id',
  authenticateToken,
  // validatePermission('UPDATE_USER'),
  UserController.updateUser
);


/**
 * @swagger
 * /api/v1/user/{id}:
 *   delete:
 *     summary: Delete user by ID
 *     description: Delete a user by their ID
 *     tags:
 *       - Users
 *     security:
 *       - BearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         description: ID of the user to delete
 *         required: true
 *         schema:
 *           type: integer
 *     responses:
 *       '204':
 *         description: No content
 *       '401':
 *         description: Unauthorized
 *       '404':
 *         description: Not found
 */
router.delete(
  '/api/v1/user/:id',
  authenticateToken,
  validatePermission('DELETE_USER'),
  UserController.deleteUser
);

/**
 * @swagger
 * /api/v1/user/validate-username/{username}:
 *   get:
 *     summary: Validate username if it exists
 *     description: Check if username is already taken
 *     tags:
 *       - Users
 *     security:
 *       - BearerAuth: []
 *     parameters:
 *       - in: path
 *         name: username
 *         description: username of the user to validate
 *         required: true
 *         schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: OK
 *       '401':
 *         description: Unauthorized
 *       '404':
 *         description: Not found
 */
router.get(
  '/api/v1/user/validate-username/:username',
  UserController.checkIfUsernameExists
);

module.exports = router;
