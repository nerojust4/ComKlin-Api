const express = require("express");
const router = express.Router();
const UserController = require("../controllers/UserController");

/**
 * @swagger
 * components:
 *   schemas:
 *     RegisterUserRequest:
 *       type: object
 *       properties:
 *         firstName:
 *           type: string
 *         lastName:
 *           type: string
 *         username:
 *           type: string
 *           minLength: 3
 *           maxLength: 30
 *         email:
 *           type: string
 *           format: email
 *         phoneNumber:
 *           type: string
 *         state:
 *           type: string
 *         country:
 *           type: string
 *         password:
 *           type: string
 *           minLength: 6
 *           maxLength: 30
 *         roleId:
 *           type: integer
 *           default: 2
 *       required:
 *         - firstName
 *         - lastName
 *         - username
 *         - email
 *         - password
 *         - roleId
 */

/**
 * @swagger
 * /api/v1/register:
 *   post:
 *     summary: Register a new user
 *     description: Register a new user with the provided details
 *     tags:
 *       - Registration
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/RegisterUserRequest'
 *     responses:
 *       '201':
 *         description: User successfully registered
 *       '400':
 *         description: Bad request or validation error
 *       '409':
 *         description: User with the provided email/username already exists
 */
router.post("/api/v1/register", UserController.createUser);

module.exports = router;
