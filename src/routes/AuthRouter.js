const express = require('express');
const router = express.Router();
const AuthController = require('../controllers/AuthController');
const { authenticateToken } = require('../middleware/AuthMiddleware');

/**
 * @swagger
 * components:
 *   schemas:
 *     LoginRequestBody:
 *       type: object
 *       properties:
 *         email:
 *           type: string
 *           description: The user's email
 *           example: "nero@example.com"
 *         password:
 *           type: string
 *           description: The user's password
 *           example: "123456"
 *         deviceId:
 *           type: string
 *           description: The client device Id
 *           example: "574JDJDN22D"
 *         appVersion:
 *           type: string
 *           description: The client's app version
 *           example: "1.0.0"
 *         phoneType:
 *           type: string
 *           description: The client's phone type
 *           example: "iOS"
 *         model:
 *           type: string
 *           description: The client's model
 *           example: "Samsung S22"
 *         client:
 *           type: string
 *           description: Who is making the request?
 *           example: "Postman"
 *       required:
 *         - email
 *         - password
 *         - deviceId
 *         - appVersion
 *         - model
 *         - client
 *     EmailRequestBody:
 *       type: object
 *       properties:
 *         email:
 *           type: string
 *           description: The user's email
 *           example: "test@example.com"
 *       required:
 *         - email
 *     ResetPasswordRequestBody:
 *       type: object
 *       properties:
 *         newPassword:
 *           type: string
 *           description: The user's new password
 *           example: "123456"
 *       required:
 *         - newPassword
 *     LoginResponse:
 *       type: object
 *       properties:
 *         status:
 *           type: boolean
 *           description: Status of the request
 *         result:
 *           type: object
 *           properties:
 *             token:
 *               type: string
 *               description: JWT token for authentication
 *             user:
 *               type: object
 *               properties:
 *                 id:
 *                   type: integer
 *                 firstName:
 *                   type: string
 *                 lastName:
 *                   type: string
 *                 username:
 *                   type: string
 *                 email:
 *                   type: string
 *                   format: email
 *                 phoneNumber:
 *                   type: string
 *                 dob:
 *                   type: string
 *                   format: date
 *                   nullable: true
 *                 gender:
 *                   type: string
 *                   nullable: true
 *                 address:
 *                   type: string
 *                 city:
 *                   type: string
 *                   nullable: true
 *                 state:
 *                   type: string
 *                 country:
 *                   type: string
 *                 active:
 *                   type: boolean
 *                 image:
 *                   type: string
 *                   nullable: true
 *                 roleId:
 *                   type: integer
 *         errors:
 *           type: array
 *           items:
 *             type: object
 *             properties:
 *               message:
 *                 type: string
 */

/**
 * @swagger
 * /api/v1/login:
 *   post:
 *     summary: Login user
 *     description: Authenticate user with provided email/username and password and generate JWT token
 *     tags:
 *       - Authentication
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/LoginRequestBody'
 *     responses:
 *       '200':
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/LoginResponse'
 *       '400':
 *         description: Bad request
 *       '401':
 *         description: Unauthorized
 */
router.post('/api/v1/login', AuthController.loginUser);

/**
 * @swagger
 * /api/v1/logout:
 *   get:
 *     summary: Logout user
 *     description: Invalidate the user's token and log the logout activity
 *     tags:
 *       - Authentication
 *     security:
 *       - BearerAuth: []
 *     responses:
 *       '200':
 *         description: Logged out successfully
 *       '401':
 *         description: Unauthorized, token invalid or expired
 *       '500':
 *         description: Internal server error
 */
router.get('/api/v1/logout', authenticateToken, AuthController.logoutUser);

/**
 * @swagger
 * /api/v1/reset-password:
 *   post:
 *     summary: Request password reset
 *     description: Request a password reset for the user
 *     tags:
 *       - Authentication
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/EmailRequestBody'
 *     responses:
 *       '200':
 *         description: Password reset requested successfully
 *       '404':
 *         description: User not found
 *       '500':
 *         description: Internal server error
 */
router.post('/api/v1/reset-password', AuthController.requestPasswordReset);

/**
 * @swagger
 * /api/v1/reset-password/{token}:
 *   post:
 *     summary: Reset password
 *     description: Reset the user's password using the provided token
 *     tags:
 *       - Authentication
 *     parameters:
 *       - in: path
 *         name: token
 *         description: The password reset token
 *         required: true
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/ResetPasswordRequestBody'
 *     responses:
 *       '200':
 *         description: Password reset successful
 *       '400':
 *         description: Bad request
 *       '401':
 *         description: Unauthorized
 *       '500':
 *         description: Internal server error
 */
router.post('/api/v1/reset-password/:token', AuthController.resetPassword);

module.exports = router;
