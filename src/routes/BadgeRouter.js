const express = require('express');
const router = express.Router();
const BadgeController = require('../controllers/BadgeController');
const { authenticateToken } = require('../middleware/AuthMiddleware');

/**
 * @swagger
 * /api/v1/badge:
 *   post:
 *     summary: Create a new badge
 *     description: Create a new badge with the provided details
 *     tags:
 *       - Badges
 *     security:
 *       - BearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *               description:
 *                 type: string
 *               minimumPosts:
 *                 type: integer
 *     responses:
 *       '201':
 *         description: Created
 *       '400':
 *         description: Bad request
 */
router.post(
  '/api/v1/badge',
  //  authenticateToken,
  BadgeController.createBadge
);

/**
 * @swagger
 * /api/v1/badge:
 *   get:
 *     summary: Get all badges
 *     description: Retrieve a list of all badges
 *     tags:
 *       - Badges
 *     parameters:
 *       - in: query
 *         name: page
 *         description: The page number to retrieve
 *         schema:
 *           type: integer
 *           default: 1
 *       - in: query
 *         name: pageSize
 *         description: The number of badges per page
 *         schema:
 *           type: integer
 *           default: 10
 *     security:
 *       - BearerAuth: []
 *     responses:
 *       '200':
 *         description: OK
 *       '401':
 *         description: Unauthorized
 */
router.get(
  '/api/v1/badge',
  // authenticateToken,
  BadgeController.getAllBadges
);

/**
 * @swagger
 * /api/v1/badge/{id}:
 *   get:
 *     summary: Get badge by ID
 *     description: Retrieve a badge by its ID
 *     tags:
 *       - Badges
 *     security:
 *       - BearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         description: ID of the badge to retrieve
 *         required: true
 *         schema:
 *           type: integer
 *     responses:
 *       '200':
 *         description: OK
 *       '401':
 *         description: Unauthorized
 *       '404':
 *         description: Not found
 */
router.get(
  '/api/v1/badge/:id',
  // authenticateToken,
  BadgeController.getBadgeById
);

/**
 * @swagger
 * /api/v1/badge/{id}:
 *   put:
 *     summary: Update badge by ID
 *     description: Update a badge by its ID
 *     tags:
 *       - Badges
 *     security:
 *       - BearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         description: ID of the badge to update
 *         required: true
 *         schema:
 *           type: integer
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *               description:
 *                 type: string
 *               minimumPosts:
 *                 type: integer
 *     responses:
 *       '200':
 *         description: OK
 *       '401':
 *         description: Unauthorized
 *       '404':
 *         description: Not found
 */
router.put('/api/v1/badge/:id', authenticateToken, BadgeController.updateBadge);

/**
 * @swagger
 * /api/v1/badge/{id}:
 *   delete:
 *     summary: Delete badge by ID
 *     description: Delete a badge by its ID
 *     tags:
 *       - Badges
 *     security:
 *       - BearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         description: ID of the badge to delete
 *         required: true
 *         schema:
 *           type: integer
 *     responses:
 *       '204':
 *         description: No content
 *       '401':
 *         description: Unauthorized
 *       '404':
 *         description: Not found
 */
router.delete(
  '/api/v1/badge/:id',
  authenticateToken,
  BadgeController.deleteBadge
);

module.exports = router;
