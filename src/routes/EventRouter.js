const express = require('express');
const router = express.Router();
const eventController = require('../controllers/eventController');
const { authenticateToken } = require('../middleware/AuthMiddleware');
const uploadMiddleware = require('../utils/Upload');

/**
 * @swagger
 * /api/v1/events:
 *   post:
 *     summary: Create a new event
 *     description: Create a new event with the provided details
 *     tags:
 *       - Events
 *     security:
 *       - BearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         multipart/form-data:
 *           schema:
 *             type: object
 *             properties:
 *               title:
 *                 type: string
 *               description:
 *                 type: string
 *               date:
 *                 type: string
 *                 format: date-time
 *               location:
 *                 type: string
 *               images:
 *                 type: array
 *                 items:
 *                   type: string
 *                   format: binary
 *               videos:
 *                 type: array
 *                 items:
 *                   type: string
 *                   format: binary
 *     responses:
 *       '201':
 *         description: Created
 *       '400':
 *         description: Bad request
 */
router.post(
  '/api/v1/events',
  authenticateToken,
  uploadMiddleware,
  eventController.createEvent
);

/**
 * @swagger
 * /api/v1/events:
 *   get:
 *     summary: Get all events
 *     description: Retrieve a list of all events
 *     tags:
 *       - Events
 *     parameters:
 *       - in: query
 *         name: page
 *         description: The page number to retrieve
 *         schema:
 *           type: integer
 *           default: 1
 *       - in: query
 *         name: pageSize
 *         description: The number of events per page
 *         schema:
 *           type: integer
 *           default: 10
 *     security:
 *       - BearerAuth: []
 *     responses:
 *       '200':
 *         description: OK
 *       '401':
 *         description: Unauthorized
 */
router.get('/api/v1/events', authenticateToken, eventController.getAllEvents);

/**
 * @swagger
 * /api/v1/events/{id}:
 *   get:
 *     summary: Get event by ID
 *     description: Retrieve an event by its ID
 *     tags:
 *       - Events
 *     security:
 *       - BearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         description: ID of the event to retrieve
 *         required: true
 *         schema:
 *           type: integer
 *     responses:
 *       '200':
 *         description: OK
 *       '401':
 *         description: Unauthorized
 *       '404':
 *         description: Not found
 */
router.get(
  '/api/v1/events/:id',
  authenticateToken,
  eventController.getEventById
);

/**
 * @swagger
 * /api/v1/events/user/{userId}:
 *   get:
 *     summary: Get events by user ID
 *     description: Retrieve a list of events by user ID
 *     tags:
 *       - Events
 *     parameters:
 *       - in: path
 *         name: userId
 *         description: ID of the user whose events to retrieve
 *         required: true
 *         schema:
 *           type: integer
 *       - in: query
 *         name: page
 *         description: The page number to retrieve
 *         schema:
 *           type: integer
 *           default: 1
 *       - in: query
 *         name: pageSize
 *         description: The number of events per page
 *         schema:
 *           type: integer
 *           default: 10
 *     security:
 *       - BearerAuth: []
 *     responses:
 *       '200':
 *         description: OK
 *       '401':
 *         description: Unauthorized
 */
router.get(
  '/api/v1/events/user/:userId',
  authenticateToken,
  eventController.getEventsByUserId
);

/**
 * @swagger
 * /api/v1/events/{id}:
 *   put:
 *     summary: Update event by ID
 *     description: Update an event by its ID
 *     tags:
 *       - Events
 *     security:
 *       - BearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         description: ID of the event to update
 *         required: true
 *         schema:
 *           type: integer
 *     requestBody:
 *       required: true
 *       content:
 *         multipart/form-data:
 *           schema:
 *             type: object
 *             properties:
 *               title:
 *                 type: string
 *               description:
 *                 type: string
 *               date:
 *                 type: string
 *                 format: date-time
 *               location:
 *                 type: string
 *               images:
 *                 type: array
 *                 items:
 *                   type: string
 *                   format: binary
 *               videos:
 *                 type: array
 *                 items:
 *                   type: string
 *                   format: binary
 *     responses:
 *       '200':
 *         description: OK
 *       '401':
 *         description: Unauthorized
 *       '404':
 *         description: Not found
 */
router.put(
  '/api/v1/events/:id',
  authenticateToken,
  uploadMiddleware,
  eventController.updateEvent
);

/**
 * @swagger
 * /api/v1/events/{id}:
 *   delete:
 *     summary: Delete event by ID
 *     description: Delete an event by its ID
 *     tags:
 *       - Events
 *     security:
 *       - BearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         description: ID of the event to delete
 *         required: true
 *         schema:
 *           type: integer
 *     responses:
 *       '204':
 *         description: No content
 *       '401':
 *         description: Unauthorized
 *       '404':
 *         description: Not found
 */
router.delete(
  '/api/v1/events/:id',
  authenticateToken,
  eventController.deleteEvent
);

module.exports = router;
