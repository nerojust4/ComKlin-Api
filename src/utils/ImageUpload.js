require("dotenv").config(); // Load environment variables from .env file

const { S3Client, PutObjectCommand } = require("@aws-sdk/client-s3");
const { Readable } = require("stream");

// Create an instance of the S3 client with the specified configuration
const s3Client = new S3Client({
  region: process.env.AWS_REGION, // Specify your AWS region
  credentials: {
    accessKeyId: process.env.SPACES_ACCESS_KEY_ID,
    secretAccessKey: process.env.SPACES_SECRET_ACCESS_KEY,
  },
  endpoint: process.env.SPACES_ENDPOINT, // Specify your DigitalOcean Spaces endpoint
});

// Function to upload an image file to DigitalOcean Spaces
const uploadImage = async (imageFile) => {
  if (!imageFile) {
    return null;
  }

  // Create a readable stream from the image file
  const fileStream = Readable.from(imageFile.buffer);

  // Define the upload parameters
  const uploadParams = {
    Bucket: process.env.SPACES_BUCKET_NAME,
    Key: imageFile.originalname, // Set the filename as the key
    Body: fileStream,
    ContentType: imageFile.mimetype,
    ACL: "public-read", // Set the file ACL to public-read
  };

  try {
    // Upload the file to DigitalOcean Spaces
    const data = await s3Client.send(new PutObjectCommand(uploadParams));
    console.log("File uploaded successfully:", data.Location);
    return data.Location; // Return the public URL of the uploaded file
  } catch (error) {
    console.error("Error uploading file:", error);
    throw new Error("Error uploading file");
  }
};

module.exports = uploadImage;
