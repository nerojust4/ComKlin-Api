const fs = require("fs");
const path = require("path");

// Directory containing API router files
const routesDir = path.join(__dirname, "../", "routes");

// Get list of API router files
const apiFiles = fs.readdirSync(routesDir)
  .filter(file => file.endsWith(".js"))
  .map(file => path.join("..", file));

// Write the list of API router files to apiList.json in the routes folder
fs.writeFileSync(path.join(__dirname, "", "", "apiList.json"), JSON.stringify(apiFiles));
