// src/utils/SocketUtil.js
class SocketUtil {
  constructor() {
    this.io = null;
  }

  initialize(server) {
    const socketIo = require('socket.io');
    this.io = socketIo(server);
  }

  getInstance() {
    if (!this.io) {
      throw new Error('Socket.io not initialized');
    }
    return this.io;
  }
}

module.exports = new SocketUtil();
