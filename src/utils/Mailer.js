const nodemailer = require('nodemailer');

const sendResetPasswordEmail = (userEmail, signedToken) => {
  return new Promise((resolve, reject) => {
    const transporter = nodemailer.createTransport({
      service: 'Gmail',
      auth: {
        user: process.env.EMAIL || 'example@gmail.com',
        pass: process.env.EMAIL_PASSWORD || '1234567'
      }
    });

    const mailOptions = {
      from: process.env.EMAIL || 'example@gmail.com',
      to: userEmail,
      subject: 'Password Reset',
      text: `To reset your password, please use the following token: ${signedToken}`
    };

    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        reject(error);
      } else {
        resolve(info);
      }
    });
  });
};

module.exports = sendResetPasswordEmail;
