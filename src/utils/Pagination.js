const getAllPaginated = async (
  model,
  page = 1,
  pageSize = 10,
  attributes = [],
  order = [['createdAt', 'DESC']],
  include = [],
  whereClause
) => {
  try {
    // Validate input parameters
    if (
      typeof page !== 'number' ||
      page < 1 ||
      typeof pageSize !== 'number' ||
      pageSize < 1
    ) {
      throw new Error('Invalid pagination parameters');
    }

    const offset = (page - 1) * pageSize;

    const { count, rows } = await model.findAndCountAll({
      include,
      attributes,
      where: whereClause, // Apply whereClause here
      limit: pageSize,
      offset,
      order
    });

    const totalPages = Math.ceil(count / pageSize);

    return { data: rows, totalPages };
  } catch (error) {
    throw error;
  }
};

module.exports = { getAllPaginated };
