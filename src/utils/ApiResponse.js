class ApiResponse {
  constructor(status, result, errors) {
    this.status = status;
    this.result = result;
    this.errors = errors;
  }

  static error(res, statusCode, message) {
    const errorResponse = new ApiResponse(false, null, [{ message }]);
    res.status(statusCode).json(errorResponse);
  }
  static errorWithArray(res, statusCode, errorMessages = []) {
    const errors = errorMessages.map((errorMessage) => ({
      message: errorMessage,
    }));
    const errorResponse = new ApiResponse(false, null, errors);
    res.status(statusCode).json(errorResponse);
  }

  static success(res, statusCode, result) {
    const response = new ApiResponse(true, result, []);
    res.status(statusCode).json(response);
  }
  static notFound(res, message) {
    const response = new ApiResponse(false, null, [{ message }]);
    res.status(404).json(response);
  }
}

module.exports = ApiResponse;
