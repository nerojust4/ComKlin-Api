const mailjet = require("node-mailjet").connect(
  process.env.MJ_APIKEY_PUBLIC,
  process.env.MJ_APIKEY_PRIVATE
);

exports.sendEmail = async (email, subject, htmlContent) => {
  try {
    const request = mailjet.post("send", { version: "v3.1" }).request({
      Messages: [
        {
          From: {
            Email: "your@email.com",
            Name: "Your Name",
          },
          To: [
            {
              Email: email,
            },
          ],
          Subject: subject,
          HTMLPart: htmlContent,
        },
      ],
    });

    const response = await request;
    console.log(response.body);
    return response.body;
  } catch (error) {
    console.error("Error sending email:", error);
    throw error;
  }
};

const resetPasswordTemplate = `
  <h3>Password Reset Request</h3>
  <p>You have requested to reset your password. Click the link below to reset your password:</p>
  <a href="${resetLink}">${resetLink}</a>
`;

const welcomeTemplate = `
  <h3>Welcome to our platform!</h3>
  <p>Thank you for joining us. We're excited to have you on board.</p>
`;

exports.sendPasswordResetEmail = async (email, resetLink) => {
  const subject = "Password Reset Request";
  return sendEmail(email, subject, resetPasswordTemplate.replace("${resetLink}", resetLink));
};

exports.sendWelcomeEmail = async (email) => {
  const subject = "Welcome to Our Platform!";
  return sendEmail(email, subject, welcomeTemplate);
};
