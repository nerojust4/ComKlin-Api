const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const ApiResponse = require('./ApiResponse');
const { User, AuditLog, Permission, RolePermission } = require('../models');
const { Op } = require('sequelize');
const crypto = require('crypto');
const nodemailer = require('nodemailer');

const generateAphaNumericResetToken = () => {
  return crypto.randomBytes(3).toString('hex'); // 3 bytes * 2 hex chars/byte = 6 characters
};
const generateNumericResetToken = () => {
  const token = Math.floor(100000 + Math.random() * 900000).toString();
  return token;
};
const signResetToken = (token) => {
  const jwtToken = jwt.sign({ resetToken: token }, process.env.JWT_SECRET, {
    expiresIn: '1h'
  });
  return jwtToken;
};

function validateUserRegistrationDataFields(req, res) {
  const requiredFields = [
    { field: 'firstName', label: 'First Name' },
    { field: 'lastName', label: 'Last Name' },
    { field: 'username', label: 'Username' },
    { field: 'email', label: 'Email' },
    { field: 'phoneNumber', label: 'Phone Number' },
    { field: 'password', label: 'Password' },
    { field: 'roleId', label: 'Role' },
    // { field: 'client', label: 'Client' },
    // { field: 'address', label: 'Address' },
    { field: 'country', label: 'Country' },
    { field: 'state', label: 'State' }
  ];
  const missingFields = requiredFields
    .filter((field) => !req.body[field.field])
    .map((field) => field.label);

  console.log('Missing fields', missingFields);

  if (missingFields.length > 0) {
    return ApiResponse.error(
      res,
      400,
      `${missingFields.join(', ')} ${
        missingFields.length > 1 ? 'are' : 'is'
      } required`
    );
  }

  // If all validations pass, return true
  return true;
}
function getClientIpAddress(req) {
  return req.headers['x-forwarded-for'] || req.socket.remoteAddress;
}
// Validator function for individual fields
function validateField(field, value) {
  if (!value) {
    throw new Error(`Missing required field: ${field}`);
  }
}

// Validator function for user registration data
async function validateUserRegistrationData(req) {
  const requiredFields = [
    { field: 'firstName', label: 'First Name' },
    { field: 'lastName', label: 'Last Name' },
    { field: 'username', label: 'Username' },
    { field: 'email', label: 'Email' },
    { field: 'phoneNumber', label: 'Phone Number' },
    { field: 'password', label: 'Password' },
    { field: 'roleId', label: 'Role' },
    // { field: 'client', label: 'Client' },
    { field: 'address', label: 'Address' },
    { field: 'country', label: 'Country' },
    { field: 'state', label: 'State' }
  ];

  requiredFields.forEach((field) => {
    validateField(field.field, req.body[field.field]);
  });
}

const findUserByUsernameOrEmail = async (options) => {
  // Check if either email or username is provided
  if (!options.email && !options.username) {
    throw new Error('Email or username must be provided');
  }

  // Initialize where conditions
  const emailWhereCondition = {};
  const usernameWhereCondition = {};

  // Add email to where condition if provided
  if (options.email) {
    emailWhereCondition.email = options.email;
  }

  // Add username to where condition if provided
  if (options.username) {
    usernameWhereCondition.username = options.username;
  }

  // Find user by email or username
  const existingUserByEmail = await User.findOne({
    where: emailWhereCondition
  });

  const existingUserByUsername = await User.findOne({
    where: usernameWhereCondition
  });

  // Return specific error message if user exists, false otherwise
  if (existingUserByEmail) {
    throw new Error('Email already exists');
  }

  if (existingUserByUsername) {
    throw new Error('Username already exists');
  }

  // Return true if user doesn't exist, false otherwise
  return !existingUserByEmail && !existingUserByUsername;
};

const findUserByEmail = async (email, username) => {
  // Check if either email or username is provided
  if (!email && !username) {
    throw new Error('Email or username must be provided');
  }

  // Define query condition based on provided email or username
  let queryCondition = {};
  if (email) {
    queryCondition = { email };
  } else {
    queryCondition = { username };
  }

  // Check if the email or username is already registered
  const existingUser = await User.findOne({
    where: queryCondition
  });

  return !!existingUser; // Convert to boolean
};
async function addAuditAction(req, action, actionId) {
  let userId = req?.user?.userId || req?.userId;
  console.log('userId in audit', userId);
  const user = await User.findByPk(userId || req.user.userId);
  if (!user) {
    throw new Error('User not found');
  }
  await AuditLog.create({
    userId: user.id,
    action: action,
    details: `${user.username} performed ${action} on entity ${actionId}`,
    client: req.headers['client'] || req.headers['user-agent'],
    ipAddress: getClientIpAddress(req),
    deviceId: req.headers['deviceid'],
    appVersion: req.headers['appversion'],
    phoneType: req.headers['phonetype'],
    model: req.headers['model'],
    systemName: req.headers['systemname'],
    systemVersion: req.headers['systemversion'],
    bundleId: req.headers['bundleid']
  });
}

const validatePermissionAccessOfRequester = async (userId, permissionName) => {
  const user = await User.findByPk(userId);
  if (!user) {
    throw new Error('User not found');
  }

  const permission = await Permission.findOne({
    where: { name: permissionName }
  });
  if (!permission) {
    throw new Error('Permission not found');
  }

  const rolePermission = await RolePermission.findOne({
    where: { roleId: user.roleId, permissionId: permission.id }
  });
  if (!rolePermission) {
    throw new Error('Permission denied, contact admin');
  }
};

module.exports = {
  generateAphaNumericResetToken,
  addAuditAction,
  generateNumericResetToken,
  validatePermissionAccessOfRequester,
  signResetToken,
  validateUserRegistrationDataFields,
  validateUserRegistrationData,
  findUserByUsernameOrEmail,
  findUserByEmail,
  getClientIpAddress
};
