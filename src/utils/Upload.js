const multer = require('multer');
const path = require('path');
const ApiResponse = require('./ApiResponse');

// Define storage for the uploaded files
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'src/uploads/'); // Directory to store uploaded files
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1e9);
    cb(null, `${uniqueSuffix}-${file.originalname}`);
  }
});

// File filter to allow only specific file types (images and videos)
const fileFilter = (req, file, cb) => {
  const allowedTypes = /jpeg|jpg|png|gif|mp4|avi|mov|wmv/;
  const mimeType = allowedTypes.test(file.mimetype);
  const extname = allowedTypes.test(
    path.extname(file.originalname).toLowerCase()
  );

  if (mimeType && extname) {
    return cb(null, true);
  } else {
    cb(new Error('Only images and videos are allowed'));
  }
};

// Initialize Multer with the defined storage and file filter
const upload = multer({
  storage: storage,
  fileFilter: fileFilter,
  limits: { fileSize: 1024 * 1024 * 20 } // 20MB limit per file
}).fields([
  { name: 'images', maxCount: 10 }, // Allow up to 10 images
  { name: 'videos', maxCount: 5 } // Allow up to 5 videos
]);

// Custom middleware to handle multer errors
const uploadMiddleware = (req, res, next) => {
  upload(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      // Multer-specific errors
      return ApiResponse.error(res, 400, err.message);
    } else if (err) {
      // Other errors
      return ApiResponse.error(res, 400, err.message);
    }
    // If no error, proceed to the next middleware/controller
    next();
  });
};

module.exports = uploadMiddleware;
