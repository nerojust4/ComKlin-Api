require("dotenv").config(); // Load environment variables from .env file
const { Sequelize } = require("sequelize");

const config = {
  database: process.env.DB_DATABASE,
  username: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  dialect: "mysql", // Or any other supported dialect
  // Add other Sequelize options as needed
};

const sequelize = new Sequelize(
  config.database,
  config.username,
  config.password,
  {
    host: config.host,
    port: config.port,
    dialect: config.dialect,
    logging: false, // Disable SQL query logging
  }
);

module.exports = { sequelize, Sequelize };
