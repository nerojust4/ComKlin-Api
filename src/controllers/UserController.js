const bcrypt = require('bcrypt');
const { User, AuditLog, UserRole, Role, Badge } = require('../models');
const { Op, where, STRING } = require('sequelize');
const ApiResponse = require('../utils/ApiResponse');
const uploadImage = require('../utils/ImageUpload');
const {
  validateUserRegistrationDataFields,
  findUserByUsernameOrEmail,
  validateUserRegistrationData,
  getClientIpAddress
} = require('../utils/Utils');
const STRINGS = require('../utils/Strings');
const { getAllPaginated } = require('../utils/Pagination');

exports.createUser = async (req, res) => {
  try {
    // Validate user input
    console.log('About to register with ', req.body);

    if (!validateUserRegistrationDataFields(req, res)) {
      return;
    }
    console.log('after validation');
    const {
      firstName,
      lastName,
      username,
      email,
      phoneNumber,
      gender,
      address,
      dob,
      state,
      city,
      country,
      password,
      roleId,
      client,
      appVersion,
      phoneType,
      deviceId,
      model,
      systemName,
      systemVersion,
      bundleId
    } = req.body;

    // Check if the email or username already exists
    await findUserByUsernameOrEmail({ email, username });

    //check the role bought in
    const role = await Role.findByPk(roleId);
    if (!role) {
      return ApiResponse.notFound(res, STRINGS.ROLE_NOT_FOUND);
    }

    const existingUsername = await User.findOne({
      where: {
        [Op.or]: [{ username }]
      }
    });

    if (existingUsername) {
      return ApiResponse.error(res, 500, 'Username already exists');
    }

    // Hash the password
    const hashedPassword = await bcrypt.hash(password, 10);
    console.log('hashed password', hashedPassword);

    // Upload the user image
    const imageFile = req.file;
    const imageUrl = await uploadImage(imageFile); // Define uploadImage function to upload image to DigitalOcean Spaces or other storage

    // Create the user
    const newUser = await User.create({
      firstName,
      lastName,
      username,
      email,
      phoneNumber,
      gender,
      city,
      state,
      country,
      password: hashedPassword,
      imageUrl: imageUrl, // Store the image URL in the user model
      roleId: roleId // Assuming role is the ID of the role assigned to the user
    });
    // Create a record in the UserRoles table to assign the user's role
    await UserRole.create({
      userId: newUser.id,
      roleId: roleId // Assuming role is the ID of the role assigned to the user
    });

    // Create a new object without the password property
    const newUserWithoutPassword = { ...newUser.toJSON() };
    delete newUserWithoutPassword.password;
    // Log successful registration
    await AuditLog.create({
      userId: newUser.id,
      action: 'REGISTRATION',
      details: `${newUser.username} registered in successfully`,
      client,
      ipAddress: getClientIpAddress(req),
      deviceId,
      appVersion,
      phoneType,
      model,
      systemName,
      systemVersion,
      bundleId
    });
    console.log('User registered successfully', newUserWithoutPassword);
    return ApiResponse.success(res, 201, newUserWithoutPassword);
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};
exports.createUserOnly = async (req, res) => {
  // console.log("About to register with ", req.body);
  await validateUserRegistrationData(req);

  const {
    firstName,
    lastName,
    username,
    email,
    phoneNumber,
    address,
    dob,
    gender,
    city,
    state,
    country,
    password,
    roleId,
    client
  } = req.body;
  console.log('Inside create user only section', req.body);

  // Check if the email or username already exists
  await findUserByUsernameOrEmail({ email, username });

  // Check if the role exists
  const role = await Role.findByPk(roleId);
  if (!role) {
    throw new Error(STRINGS.ROLE_NOT_FOUND);
  }

  const existingUsername = await User.findOne({
    where: {
      [Op.or]: [{ username }]
    }
  });

  if (existingUsername) {
    throw new Error('Username already exists');
  }

  // Hash the password
  const hashedPassword = await bcrypt.hash(password, 10);
  // console.log("hashed password", hashedPassword);

  // Upload the user image
  const imageFile = req.file;
  const imageUrl = await uploadImage(imageFile); // Define uploadImage function to upload image to DigitalOcean Spaces or other storage

  // Create the user
  const newUser = await User.create({
    firstName,
    lastName,
    username,
    email,
    phoneNumber,
    address,
    dob,
    gender,
    city,
    state,
    country,
    password: hashedPassword,
    imageUrl: imageUrl, // Store the image URL in the user model
    roleId: roleId // Assuming role is the ID of the role assigned to the user
  });

  // Create a record in the UserRoles table to assign the user's role
  await UserRole.create({
    userId: newUser.id,
    roleId: roleId // Assuming role is the ID of the role assigned to the user
  });

  // Create a new object without the password property
  const newUserWithoutPassword = { ...newUser.toJSON() };
  delete newUserWithoutPassword.password;

  // Log successful registration
  await AuditLog.create({
    userId: newUser.id,
    action: STRINGS.REGISTRATION,
    details: `${newUser.username} registered successfully`,
    client: client,
    ipAddress: req.ip
  });

  // console.log("User registered successfully", newUserWithoutPassword);
  return newUserWithoutPassword;
};

exports.checkIfUsernameExists = async (req, res) => {
  try {
    const username = req.params.username;

    const existingUser = await User.findOne({
      where: {
        [Op.or]: [{ username }]
      }
    });
    if (existingUser) {
      return ApiResponse.success(res, 200, { isUserExists: true });
    }

    return ApiResponse.success(res, 200, { isUserExists: false });
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};

exports.getAllUsers = async (req, res) => {
  try {
    // Parse query parameters and set defaults
    const page = parseInt(req.query.page) || 1;
    const pageSize = parseInt(req.query.pageSize) || 10;
    const keywords = req.query.keywords || '';
    const fetchAll = req.query.all === 'true';

    console.log(
      'About to get all users',
      'page',
      page,
      'page size',
      pageSize,
      'search keywords',
      keywords,
      'fetch all',
      fetchAll
    );

    // Build the where clause for search
    const whereClause = keywords
      ? {
          [Op.or]: [
            { firstName: { [Op.like]: `%${keywords}%` } },
            { lastName: { [Op.like]: `%${keywords}%` } },
            { username: { [Op.like]: `%${keywords}%` } },
            { email: { [Op.like]: `%${keywords}%` } }
            // Add additional fields to search here
          ]
        }
      : {};

    let users, totalPages;

    if (fetchAll) {
      // Fetch all users without pagination
      users = await User.findAll({
        where: whereClause,
        attributes: ['id', 'firstName', 'lastName', 'username', 'email'],
        order: [['createdAt', 'DESC']],
        include: [
          {
            model: Badge,
            as: 'badge',
            attributes: ['id', 'name'],
            include: [
              {
                model: Role,
                as: 'role',
                attributes: ['id', 'name']
              }
            ]
          }
        ]
      });
      totalPages = 1; // Since we're fetching all users, there's only one "page"
    } else {
      // Fetch paginated users
      const result = await getAllPaginated(
        User,
        page,
        pageSize,
        null,
        [['createdAt', 'DESC']],
        [
          {
            model: Badge,
            as: 'badge', // Alias for the association
            attributes: ['id', 'name']
          },
          {
            model: Role,
            as: 'role', // Assuming User has a role association
            attributes: ['id', 'name']
          }
        ],
        whereClause
      );
      users = result.data;
      totalPages = result.totalPages;
    }

    console.log('Users', users.length, 'total pages', totalPages);

    return ApiResponse.success(res, 200, { users, totalPages });
  } catch (error) {
    // Handle errors
    console.error('Error while fetching users:', error);
    return ApiResponse.error(
      res,
      500,
      error.message || 'Internal Server Error'
    );
  }
};

exports.getUserById = async (req, res) => {
  try {
    const user = await User.findByPk(req.params.id, {
      attributes: {
        exclude: ['password', 'createdAt', 'updatedAt'] // Specify attributes to exclude
      }
    });
    if (!user) {
      return ApiResponse.notFound(res, STRINGS.USER_NOT_FOUND);
    }
    return ApiResponse.success(res, 200, user);
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};

exports.updateUser = async (req, res) => {
  try {
    const userId = req.params.id;
    let imageUrl;
    console.log('About to update user', req.params.id, req.body);
    // Find the user by ID
    const user = await User.findByPk(userId);
    if (!user) {
      return ApiResponse.notFound(res, STRINGS.USER_NOT_FOUND);
    }
    // Check if the request contains a file for image update
    if (req.file) {
      // Upload the new image to DigitalOcean Spaces
      imageUrl = await uploadImage(req.file);

      // If the image upload fails, return an error response
      if (!imageUrl) {
        return ApiResponse.error(res, 500, 'Failed to upload image');
      }
    }
    let object = {};
    if (req.body.email) {
      object.email = req.body.email;
    }
    if (req.body.phoneNumber) {
      object.phoneNumber = req.body.phoneNumber;
    }
    console.log('object', object);
    // Update the user's fields based on the request body
    await user.update({
      ...object,
      imageUrl: imageUrl || user.imageUrl // Use the new image URL if available, otherwise keep the existing one
    });
    const updatedUser = await User.findByPk(userId);
    console.log('Succesfully updated the user', updatedUser.dataValues);
    return ApiResponse.success(res, 200, updatedUser);
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};

exports.deleteUser = async (req, res) => {
  try {
    console.log('About to delete user', req.params);

    const { id } = req.params;
    const user = await User.findByPk(id);
    if (!user) {
      return ApiResponse.notFound(res, STRINGS.USER_NOT_FOUND);
    }

    await user.destroy();
    return ApiResponse.success(res, 200, 'Deleted successfully');
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};
