const { Role, Permission, RolePermission } = require('../models');
const ApiResponse = require('../utils/ApiResponse');
const STRINGS = require('../utils/Strings');
const { getAllPaginated } = require('../utils/Pagination');
const { Op } = require('sequelize');

exports.createRole = async (req, res) => {
  try {
    const role = await Role.create(req.body);
    return ApiResponse.success(res, 201, role);
  } catch (error) {
    return ApiResponse.error(res, 400, error.message);
  }
};

exports.getAllRoles = async (req, res) => {
  try {
    const page = parseInt(req.query.page) || 1;
    const pageSize = parseInt(req.query.pageSize) || 10;

    const { data: roles, totalPages } = await getAllPaginated(
      Role,
      page,
      pageSize,
      null, // Empty attributes array to include all columns
      [['createdAt', 'DESC']], // Order by createdAt descending
      [], // No additional include associations needed
      {
        name: {
          [Op.ne]: 'Superadmin' // Exclude roles where name is 'Superadmin'
        }
      }
    );

    return ApiResponse.success(res, 200, { roles, totalPages });
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};

exports.getRoleById = async (req, res) => {
  try {
    const role = await Role.findByPk(req.params.id);
    if (!role) {
      return ApiResponse.notFound(res, STRINGS.ROLE_NOT_FOUND);
    }
    return ApiResponse.success(res, 200, role);
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};

exports.updateRole = async (req, res) => {
  try {
    const role = await Role.findByPk(req.params.id);
    if (!role) {
      return ApiResponse.notFound(res, STRINGS.ROLE_NOT_FOUND);
    }
    await role.update(req.body);
    return ApiResponse.success(res, 200, role);
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};

exports.deleteRole = async (req, res) => {
  try {
    const role = await Role.findByPk(req.params.id);
    if (!role) {
      return ApiResponse.notFound(res, STRINGS.ROLE_NOT_FOUND);
    }
    await role.destroy();
    return ApiResponse.success(res, 204, STRINGS.DELETED_SUCESSFULLY);
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};

//permissions
exports.createPermission = async (req, res) => {
  try {
    const permission = await Permission.create(req.body);
    return ApiResponse.success(res, 201, permission);
  } catch (error) {
    return ApiResponse.error(res, 400, error.message);
  }
};

exports.getAllPermissions = async (req, res) => {
  try {
    const page = parseInt(req.query.page) || 1;
    const pageSize = parseInt(req.query.pageSize) || 10;

    const { data: permissions, totalPages } = await getAllPaginated(
      Permission,
      page,
      pageSize,
      { include: [], exclude: [] } // Specify empty arrays to include all columns
    );

    return ApiResponse.success(res, 200, { permissions, totalPages });
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};

exports.getPermissionById = async (req, res) => {
  try {
    const permission = await Permission.findByPk(req.params.id);
    if (!permission) {
      return ApiResponse.notFound(res, STRINGS.ROLE_NOT_FOUND);
    }
    return ApiResponse.success(res, 200, permission);
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};

exports.updatePermission = async (req, res) => {
  try {
    const permission = await Permission.findByPk(req.params.id);
    if (!permission) {
      return ApiResponse.notFound(res, STRINGS.ROLE_NOT_FOUND);
    }
    await permission.update(req.body);
    return ApiResponse.success(res, 200, permission);
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};

exports.deletePermission = async (req, res) => {
  try {
    const permission = await Permission.findByPk(req.params.id);
    if (!permission) {
      return ApiResponse.notFound(res, STRINGS.ROLE_NOT_FOUND);
    }
    await permission.destroy();
    return ApiResponse.success(res, 204, STRINGS.DELETED_SUCESSFULLY);
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};

//permissions
exports.createRolePermission = async (req, res) => {
  try {
    const permission = await RolePermission.create(req.body);
    return ApiResponse.success(res, 201, permission);
  } catch (error) {
    return ApiResponse.error(res, 400, error.message);
  }
};

exports.getAllRolePermissions = async (req, res) => {
  try {
    const page = parseInt(req.query.page) || 1;
    const pageSize = parseInt(req.query.pageSize) || 10;
    const keywords = req.query.keywords || '';
    const fetchAll = req.query.all === 'true';

    const whereClause = keywords
      ? {
          [Op.or]: [
            { '$permission.name$': { [Op.like]: `%${keywords}%` } },
            { '$role.name$': { [Op.like]: `%${keywords}%` } }
          ]
        }
      : {};
    let permissions, totalPages;
    if (fetchAll) {
      // Fetch all permissions without pagination
      permissions = await RolePermission.findAll({
        order: [['createdAt', 'DESC']],
        attributes: null,
        include: [
          {
            model: Permission,
            as: 'permission',
            attributes: ['id', 'name']
          },
          {
            model: Role,
            as: 'role',
            attributes: ['id', 'name']
          }
        ],
        where: whereClause
      });
      totalPages = 1; // If fetching all, consider it as a single page
    } else {
      const result = await getAllPaginated(
        RolePermission,
        page,
        pageSize,
        null,
        [['createdAt', 'DESC']],
        [
          {
            model: Permission,
            as: 'permission',
            attributes: ['id', 'name']
          },
          {
            model: Role,
            as: 'role',
            attributes: ['id', 'name']
          }
        ],
        whereClause
      );
      permissions = result.data;
      totalPages = result.totalPages;
    }
    return ApiResponse.success(res, 200, { permissions, totalPages });
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};

exports.getRolePermissionById = async (req, res) => {
  try {
    const permission = await RolePermission.findByPk(req.params.id);
    if (!permission) {
      return ApiResponse.notFound(res, STRINGS.ROLE_NOT_FOUND);
    }
    return ApiResponse.success(res, 200, permission);
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};

exports.updateRolePermission = async (req, res) => {
  try {
    const permission = await RolePermission.findByPk(req.params.id);
    if (!role) {
      return ApiResponse.notFound(res, STRINGS.ROLE_NOT_FOUND);
    }
    await permission.update(req.body);
    return ApiResponse.success(res, 200, permission);
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};

exports.deleteRolePermission = async (req, res) => {
  try {
    const permission = await RolePermission.findByPk(req.params.id);
    if (!role) {
      return ApiResponse.notFound(res, STRINGS.ROLE_NOT_FOUND);
    }
    await permission.destroy();
    return ApiResponse.success(res, 204, STRINGS.DELETED_SUCESSFULLY);
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};
