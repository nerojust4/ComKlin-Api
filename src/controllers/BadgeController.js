// controllers/badgeController.js

const ApiResponse = require('../utils/ApiResponse');
const { Badge, User } = require('../models');
const { getAllPaginated } = require('../utils/Pagination');

exports.assignBadge = async (userId) => {
  try {
    const user = await User.findByPk(userId);
    if (!user) {
      throw new Error('User not found');
    }

    const badges = await Badge.findAll();
    let assignedBadge = null;

    for (const badge of badges) {
      if (user.posts.length >= badge.minimumPosts) {
        assignedBadge = badge;
        break;
      }
    }

    if (assignedBadge) {
      await user.setBadge(assignedBadge);
      return assignedBadge;
    }

    return null; // No badge assigned
  } catch (error) {
    throw new Error(`Failed to assign badge: ${error.message}`);
  }
};

exports.createBadge = async (req, res) => {
  try {
    const badge = await Badge.create(req.body);
    return ApiResponse.success(res, 201, badge);
  } catch (error) {
    return ApiResponse.error(res, 400, error.message);
  }
};

exports.getAllBadges = async (req, res) => {
  console.log('About to get all badges');
  try {
    const page = parseInt(req.query.page) || 1;
    const pageSize = parseInt(req.query.pageSize) || 10;

    const { data: badges, totalPages } = await getAllPaginated(
      Badge,
      page,
      pageSize,
      null, // Empty attributes array to include all columns
      [['createdAt', 'DESC']], // Order by createdAt descending
      [],
      // [{ model: PostMedia, as: 'media' }], // Include media association
      {}
    );

    console.log('Got posts', badges.length);
    return ApiResponse.success(res, 200, { badges, totalPages });
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};

exports.getBadgeById = async (req, res) => {
  try {
    const badge = await Badge.findByPk(req.params.id);
    if (!badge) {
      return ApiResponse.notFound(res, 'Badge not found');
    }
    return ApiResponse.success(res, 200, badge);
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};

exports.updateBadge = async (req, res) => {
  try {
    const badge = await Badge.findByPk(req.params.id);
    if (!badge) {
      return ApiResponse.notFound(res, 'Badge not found');
    }
    await badge.update(req.body);
    return ApiResponse.success(res, 200, badge);
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};

exports.deleteBadge = async (req, res) => {
  try {
    const badge = await Badge.findByPk(req.params.id);
    if (!badge) {
      return ApiResponse.notFound(res, 'Badge not found');
    }
    await badge.destroy();
    return ApiResponse.success(res, 204, 'Badge deleted successfully');
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};
