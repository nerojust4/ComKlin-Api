const { Event, EventUser, EventMedia } = require('../models');
const ApiResponse = require('../utils/ApiResponse');
const STRINGS = require('../utils/Strings');
const { getAllPaginated } = require('../utils/Pagination');
const path = require('path');
const SocketUtil = require('../utils/SocketUtil');

exports.createEvent = async (req, res) => {
  const io = SocketUtil.getInstance();

  try {
    console.log('About to create an event', req.files, req.body);

    // Extract images and videos from req.files
//     const images = req?.files['images'] || [];
//     const videos = req?.files['videos'] || [];

//     let imageUrls = [];
//     let videoUrls = [];
// console.log("start")
//     if (images.length > 0) {
//       imageUrls = images.map(
//         (file) =>
//           `${req.protocol}://${req.get('host')}/src/uploads/${path.basename(
//             file.filename
//           )}`
//       );
//     }
//     if (videoUrls.length > 0) {
//        videoUrls = videos.map(
//         (file) =>
//           `${req.protocol}://${req.get('host')}/src/uploads/${path.basename(
//             file.filename
//           )}`
//       );
//     }
console.log("here 1")
    const event = await Event.create({
      ...req.body
    });

    // Only proceed with media-related operations if there are images or videos
    // if (imageUrls.length > 0 || videoUrls.length > 0) {
    //   console.log("here o")
    //   const mediaPromises = [
    //     ...imageUrls.map((url) =>
    //       EventMedia.create({
    //         mediaType: 'image',
    //         url,
    //         eventId: event.id
    //       })
    //     ),
    //     ...videoUrls.map((url) =>
    //       EventMedia.create({
    //         mediaType: 'video',
    //         url,
    //         eventId: event.id
    //       })
    //     )
    //   ];

    //   await Promise.all(mediaPromises);
    // }

    console.log('user id', req.user);
    // Create entry in EventUser table
    await EventUser.create({
      eventId: event.id,
      userId: req.user.userId
    });

    console.log('Event created successfully', event.dataValues);
    io.emit('eventCreated', event); // Emit eventCreated

    return ApiResponse.success(res, 201, event);
  } catch (error) {
    return ApiResponse.error(res, 400, error.message);
  }
};

exports.getAllEvents = async (req, res) => {
  console.log('About to get all events');
  try {
    const page = parseInt(req.query.page) || 1;
    const pageSize = parseInt(req.query.pageSize) || 10;

    const { data: events, totalPages } = await getAllPaginated(
      Event,
      page,
      pageSize,
      null, // Empty attributes array to include all columns
      [['date', 'ASC']], // Order by date ascending
      [], // No additional include associations needed
      {}
    );

    console.log('Got events', events.length);
    return ApiResponse.success(res, 200, { events, totalPages });
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};

exports.getEventsByUserId = async (req, res) => {
  try {
    const { userId } = req.params;
    const page = parseInt(req.query.page) || 1;
    const pageSize = parseInt(req.query.pageSize) || 10;

    const { count, rows: eventUsers } = await EventUser.findAndCountAll({
      where: { userId },
      limit: pageSize,
      offset: (page - 1) * pageSize,
      include: [
        {
          model: Event
        }
      ]
    });

    const totalPages = Math.ceil(count / pageSize);

    const events = eventUsers.map((eventUser) => eventUser.Event);

    return ApiResponse.success(res, 200, { events, totalPages });
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};

exports.getEventById = async (req, res) => {
  try {
    const event = await Event.findByPk(req.params.id);
    if (!event) {
      return ApiResponse.notFound(res, STRINGS.EVENT_NOT_FOUND);
    }
    return ApiResponse.success(res, 200, event);
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};

exports.updateEvent = async (req, res) => {
  try {
    const event = await Event.findByPk(req.params.id);
    if (!event) {
      return ApiResponse.notFound(res, STRINGS.EVENT_NOT_FOUND);
    }

    const existingMediaFiles = event.mediaFiles || [];
    const newImages = req.files['images'] || [];
    const newVideos = req.files['videos'] || [];
    const newMediaFiles = [...newImages, ...newVideos].map(
      (file) =>
        `${req.protocol}://${req.get('host')}/src/uploads/${path.basename(
          file.filename
        )}`
    );
    const mediaFiles = [...existingMediaFiles, ...newMediaFiles];

    await event.update({
      ...req.body,
      mediaFiles
    });

    io.emit('eventUpdated', event); // Emit eventUpdated

    return ApiResponse.success(res, 200, event);
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};

exports.deleteEvent = async (req, res) => {
  try {
    const event = await Event.findByPk(req.params.id);
    if (!event) {
      return ApiResponse.notFound(res, STRINGS.EVENT_NOT_FOUND);
    }
    await event.destroy();
    io.emit('eventDeleted', event.id); // Emit eventDeleted

    return ApiResponse.success(res, 204, STRINGS.DELETED_SUCESSFULLY);
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};
