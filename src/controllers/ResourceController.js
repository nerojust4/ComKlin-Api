const {
  PollutionResource,
  PollutionResourceLink,
  Resource
} = require('../models');
const ApiResponse = require('../utils/ApiResponse');
const { getAllPaginated } = require('../utils/Pagination');
const { Op } = require('sequelize');

// Create a pollution resource
exports.createResource = async (req, res) => {
  try {
    const resource = await PollutionResource.create(req.body);
    return ApiResponse.success(res, 201, resource);
  } catch (error) {
    return ApiResponse.error(res, 400, error.message);
  }
};

// Get all pollution resources with pagination
exports.getAllResources = async (req, res) => {
  try {
    const page = parseInt(req.query.page) || 1;
    const pageSize = parseInt(req.query.pageSize) || 10;

    const { data: resources, totalPages } = await getAllPaginated(
      Resource,
      page,
      pageSize,
      null, // Empty attributes array to include all columns
      [['createdAt', 'DESC']], // Order by createdAt descending
      [], // No additional include associations needed
      {
        category: {
          [Op.in]: ['air', 'water'] // Filter by categories air and water
        }
      }
    );

    return ApiResponse.success(res, 200, { resources, totalPages });
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};

// Get a pollution resource by ID
exports.getResourceById = async (req, res) => {
  try {
    const resource = await PollutionResource.findByPk(req.params.id);
    if (!resource) {
      return ApiResponse.notFound(res, 'Pollution resource not found');
    }
    return ApiResponse.success(res, 200, resource);
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};

// Update a pollution resource
exports.updateResource = async (req, res) => {
  try {
    const resource = await PollutionResource.findByPk(req.params.id);
    if (!resource) {
      return ApiResponse.notFound(res, 'Pollution resource not found');
    }
    await resource.update(req.body);
    return ApiResponse.success(res, 200, resource);
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};

// Delete a pollution resource
exports.deleteResource = async (req, res) => {
  try {
    const resource = await PollutionResource.findByPk(req.params.id);
    if (!resource) {
      return ApiResponse.notFound(res, 'Pollution resource not found');
    }
    await resource.destroy();
    return ApiResponse.success(res, 204, 'Deleted successfully');
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};
