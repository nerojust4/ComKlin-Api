const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {
  User,
  AuditLog,
  BlacklistedToken,
  Session,
  Role,
  RoleFeature,
  Feature,
  Badge
} = require('../models');
const {
  LOGIN,
  USER_NOT_FOUND,
  USER_NOT_EXIST,
  USER_DEACTIVATED,
  USER_LOGGED_OUT,
  EMAIL_NOT_EXIST,
  EMAIL_PASSWORD_REQUIRED,
  LOGOUT,
  LOGOUT_SUCCESSFUL,
  RESET_EMAIL_SENT,
  INVALID_TOKEN,
  RESET_PASSWORD_SUCCESSFUL,
  INVALID_PASSWORD,
  USER_INACTIVE
} = require('../utils/Strings');

const ApiResponse = require('../utils/ApiResponse');
const {
  generateResetToken,
  findUserByUsernameOrEmail,
  findUserByEmail,
  getClientIpAddress,
  addAuditAction
} = require('../utils/Utils');
const { sendResetPasswordEmail } = require('../utils/Mailer');

exports.loginUser = async (req, res) => {
  // console.log('login header request', req.headers);
  try {
    // Validate user input
    console.log('About to login with ', req.body);
    const { email, password, model, client } = req.body;
    // console.log('ip addresss is', ip);
    if (!email || !password) {
      return ApiResponse.error(res, 400, EMAIL_PASSWORD_REQUIRED);
    }

    // Check if the email or username already exists
    if (!(await findUserByEmail(email))) {
      console.log('Email does not exist');
      return ApiResponse.error(res, 400, EMAIL_NOT_EXIST);
    }
    // Check if the user exists
    const user = await User.findOne({
      where: { email },
      attributes: {
        exclude: ['createdAt', 'updatedAt'] // Specify attributes to exclude
      }
    });
    const role = await Role.findByPk(user.roleId);
    if (!role) {
      return ApiResponse.error(res, 400, 'role not found');
    }

    if (!user) {
      return ApiResponse.error(res, 400, USER_NOT_EXIST);
    }

    // Check if the user is active
    // if (!user.active) {
    //   return ApiResponse.error(res, 400, USER_INACTIVE);
    // }

    // Verify the password
    const passwordMatch = await bcrypt.compare(password, user.password);
    if (!passwordMatch) {
      return ApiResponse.error(res, 400, INVALID_PASSWORD);
    }

    // Generate JWT token
    const token = jwt.sign({ userId: user.id }, process.env.JWT_SECRET, {
      expiresIn: process.env.JWT_EXPIRY
    });

    // Decode the JWT token to extract expiration time
    const decodedToken = jwt.decode(token);

    // Extract the expiration time from the decoded token
    const expiresAt = new Date(decodedToken.exp * 1000); // Convert seconds to milliseconds

    // Create a session record with the expiration time from the token
    await Session.create({
      userId: user.id,
      token,
      expiresAt
    });
    //doing this for the audit to find the id
    req.userId = user.id;
    //here i mean
    addAuditAction(req, LOGIN, null);

    // Create a new object without the password property
    const userWithoutPassword = { ...user.toJSON() };
    delete userWithoutPassword.password;
    delete userWithoutPassword.client;
    userWithoutPassword.role = role;

    const badge = await Badge.findOne({
      where: { id: user.badgeId },
      attributes: {
        exclude: ['createdAt', 'updatedAt'] // Specify attributes to exclude
      }
    });
    userWithoutPassword.badge = badge
    // const roleFeatures = await RoleFeature.findAll({
    //   where: { roleId: role.id },
    //   include: [
    //     {
    //       model: Feature,
    //       as: 'feature', // Specify the alias if exists
    //       attributes: ['name'] // Only include the 'name' attribute
    //     }
    //   ]
    // });
    // //create a new array from it based on the name
    // const featureNames = roleFeatures.map((rf) => rf.feature.name); // Accessing through the alias
    // //add the param to the object
    // userWithoutPassword.features = featureNames;

    return ApiResponse.success(res, 200, {
      token,
      user: userWithoutPassword
    });
  } catch (error) {
    console.log('login error', error);
    return ApiResponse.error(res, 500, error.message);
  }
};

exports.logoutUser = async (req, res) => {
  // Log the logout activity in the audit table
  console.log('logout user', req.user);
  await AuditLog.create({
    userId: req.user.userId, // Assuming you have access to the user ID from the token
    action: LOGOUT,
    details: USER_LOGGED_OUT
  });

  try {
    const token = req.headers.authorization.split(' ')[1]; // Extract token from header
    await BlacklistedToken.create({ token, reason: 'User logout' });
    // Send a success response
    console.log('Logged out successfully');
    return ApiResponse.success(res, 200, { message: LOGOUT_SUCCESSFUL });
  } catch (error) {
    // Handle errors
    return ApiResponse.error(res, 500, error.message);
  }
};

//Request password reset
exports.requestPasswordReset = async (req, res) => {
  console.log('About to reset password for email', req.body);
  try {
    const { email } = req.body;
    const user = await User.findOne({ where: { email } });
    if (!user) {
      return ApiResponse.error(res, 404, 'User not found');
    }
    // Generate reset token and store it in the database
    const resetToken = generateResetToken(); // or generateAlphanumericResetToken()
    user.resetToken = resetToken;
    user.resetTokenExpires = Date.now() + 3600000; // Token expires in 1 hour
    await user.save();
    // Send reset password email containing the token
    const emailSubject = 'Password Reset';
    const emailText = `To reset your password, please use the following token: ${resetToken}`;
    const emailResponse = await sendResetPasswordEmail(email, resetToken);
    console.log('Reset email sent', user);
    if (emailResponse.success) {
      return ApiResponse.success(res, 200, {
        statusMessage: 'RESET_EMAIL_SENT'
      });
    } else {
      return ApiResponse.error(res, 500, 'Failed to send reset email');
    }
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};

// password reset
exports.resetPassword = async (req, res) => {
  // const { token } = req.params;
  const { newPassword, token } = req.body;
  const user = await User.findOne({ where: { resetToken: token } });
  if (!user) {
    return ApiResponse.error(res, 400, 'No token found');
  }
  if (user.resetTokenExpires < Date.now()) {
    return ApiResponse.error(res, 400, 'Token has expired');
  }
  // Update user's password and clear reset token
  user.password = await bcrypt.hash(newPassword, 10);
  user.resetToken = null;
  user.resetTokenExpires = null;
  await user.save();
  return ApiResponse.success(res, 200, RESET_PASSWORD_SUCCESSFUL);
};
