const { Post, PostMedia } = require('../models');
const ApiResponse = require('../utils/ApiResponse');
const STRINGS = require('../utils/Strings');
const { getAllPaginated } = require('../utils/Pagination');
const path = require('path');
const SocketUtil = require('../utils/SocketUtil');

exports.createPost = async (req, res) => {
  const io = SocketUtil.getInstance();

  try {
    console.log('About to create a post', req.files, req.body);

    // Extract images and videos from req.files
    const images = req.files['images'] || [];
    const videos = req.files['videos'] || [];

    const imageUrls = images.map(
      (file) =>
        `${req.protocol}://${req.get('host')}/src/uploads/${path.basename(
          file.filename
        )}`
    );

    const videoUrls = videos.map(
      (file) =>
        `${req.protocol}://${req.get('host')}/src/uploads/${path.basename(
          file.filename
        )}`
    );

    const post = await Post.create({
      ...req.body
    });

    const mediaPromises = [
      ...imageUrls.map((url) =>
        PostMedia.create({
          mediaType: 'image',
          url,
          postId: post.id
        })
      ),
      ...videoUrls.map((url) =>
        PostMedia.create({
          mediaType: 'video',
          url,
          postId: post.id
        })
      )
    ];

    await Promise.all(mediaPromises);

    console.log('Post created successfully', post.dataValues);
    io.emit('postCreated', post); // Emit postCreated

    return ApiResponse.success(res, 201, post);
  } catch (error) {
    return ApiResponse.error(res, 400, error.message);
  }
};

exports.updatePost = async (req, res) => {
  const io = SocketUtil.getInstance();
  try {
    const post = await Post.findByPk(req.params.id);
    if (!post) {
      return ApiResponse.notFound(res, STRINGS.POST_NOT_FOUND);
    }

    const newImages = req.files['images'] || [];
    const newVideos = req.files['videos'] || [];

    const imageUrls = newImages.map(
      (file) =>
        `${req.protocol}://${req.get('host')}/src/uploads/${path.basename(
          file.filename
        )}`
    );

    const videoUrls = newVideos.map(
      (file) =>
        `${req.protocol}://${req.get('host')}/src/uploads/${path.basename(
          file.filename
        )}`
    );

    await post.update({
      ...req.body
    });

    const mediaPromises = [
      ...imageUrls.map((url) =>
        PostMedia.create({
          mediaType: 'image',
          url,
          postId: post.id
        })
      ),
      ...videoUrls.map((url) =>
        PostMedia.create({
          mediaType: 'video',
          url,
          postId: post.id
        })
      )
    ];

    await Promise.all(mediaPromises);

    const updatedPost = await Post.findByPk(post.id, {
      include: [{ model: PostMedia, as: 'media' }]
    });

    console.log('Post updated successfully', updatedPost.dataValues);
    io.emit('postUpdated', updatedPost); // Emit postUpdated

    return ApiResponse.success(res, 200, updatedPost);
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};

exports.getAllPosts = async (req, res) => {
  console.log('About to get all posts');
  try {
    const page = parseInt(req.query.page) || 1;
    const pageSize = parseInt(req.query.pageSize) || 10;

    const { data: posts, totalPages } = await getAllPaginated(
      Post,
      page,
      pageSize,
      null, // Empty attributes array to include all columns
      [['createdAt', 'DESC']], // Order by createdAt descending
      [{ model: PostMedia, as: 'media' }], // Include media association
      {}
    );

    console.log('Got posts', posts.length);
    return ApiResponse.success(res, 200, { posts, totalPages });
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};

exports.getPostById = async (req, res) => {
  try {
    const post = await Post.findByPk(req.params.id, {
      include: [{ model: PostMedia, as: 'media' }]
    });
    if (!post) {
      return ApiResponse.notFound(res, STRINGS.POST_NOT_FOUND);
    }
    return ApiResponse.success(res, 200, post);
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};

exports.deletePost = async (req, res) => {
  const io = SocketUtil.getInstance();
  try {
    const post = await Post.findByPk(req.params.id);
    if (!post) {
      return ApiResponse.notFound(res, STRINGS.POST_NOT_FOUND);
    }
    await post.destroy();
    io.emit('postDeleted', post.id); // Emit postDeleted

    return ApiResponse.success(res, 204, STRINGS.DELETED_SUCESSFULLY);
  } catch (error) {
    return ApiResponse.error(res, 500, error.message);
  }
};
