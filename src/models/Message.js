const Message = (sequelize, DataTypes) => {
  const message = sequelize.define("Message", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    content: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    senderId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    receiverId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    replyTo: {
      type: DataTypes.INTEGER, // ID of the message being replied to
    },
    status: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    referenceId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    pushNotificationId: {
      type: DataTypes.STRING,
      allowNull: true,
    },
  });
  return message;
};

module.exports = Message;
