const PushNotification = (sequelize, DataTypes) => {
  return sequelize.define("PushNotification", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    message: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    recipients: {
      type: DataTypes.TEXT, // Change data type to TEXT
      allowNull: false,
      get() {
        const value = this.getDataValue("recipients");
        return value ? JSON.parse(value) : []; // Parse JSON string to array
      },
      set(value) {
        this.setDataValue("recipients", JSON.stringify(value)); // Convert array to JSON string
      },
    },
  });
};

module.exports = PushNotification;
