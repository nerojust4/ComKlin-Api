const { User } = require(".");

const PollutionReport = (sequelize, DataTypes) => {
  const PollutionReport = sequelize.define('PollutionReport', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false
    },
    latitude: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    longitude: {
      type: DataTypes.FLOAT,
      allowNull: false
    }
    //   userId: {
    //     type: DataTypes.INTEGER,
    //     references: {
    //       model: User,
    //       key: 'id'
    //     }
    //   }
  });



  return PollutionReport;
};

module.exports = PollutionReport;
