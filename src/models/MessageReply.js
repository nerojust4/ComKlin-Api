const MessageReply = (sequelize, DataTypes) => {
  const messageReply = sequelize.define("MessageReply", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    messageId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    content: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  });
  return messageReply;
};

module.exports = MessageReply;
