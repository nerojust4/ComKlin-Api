const Administrator = (sequelize, DataTypes) => {
  return sequelize.define("Administrator", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING,
    },
  });
};
module.exports = Administrator;
