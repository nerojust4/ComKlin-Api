const EventMedia = (sequelize, DataTypes) => {
  const EventMedia = sequelize.define('EventMedia', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    mediaType: {
      type: DataTypes.STRING,
      allowNull: false
    },
    url: {
      type: DataTypes.STRING,
      allowNull: false
    },
    eventId: {
      type: DataTypes.INTEGER
    }
  });
  return EventMedia;
};

module.exports = EventMedia;
