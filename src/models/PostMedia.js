const PostMedia = (sequelize, DataTypes) => {
  const PostMedia = sequelize.define('PostMedia', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    mediaType: {
      type: DataTypes.STRING,
      allowNull: false
    },
    url: {
      type: DataTypes.STRING,
      allowNull: false
    },
    postId: {
      type: DataTypes.INTEGER
    }
  });
  return PostMedia;
};

module.exports = PostMedia;
