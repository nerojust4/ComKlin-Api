const AppConfig = (sequelize, DataTypes) => {
  return sequelize.define('AppConfig', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    label: {
      type: DataTypes.STRING,
      allowNull: false
    },
    title: {
      type: DataTypes.STRING,
      allowNull: true
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true
    },
    isCarousel: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    isDashboard: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },

    image: {
      type: DataTypes.STRING,
      allowNull: true
    }
  });
};

module.exports = AppConfig;
