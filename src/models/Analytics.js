const Analytics = (sequelize, DataTypes) => {
  return sequelize.define("Analytics", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    metricName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    value: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    // Add other attributes as needed
  });
};
module.exports = Analytics;
