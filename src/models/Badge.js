// models/Event.js

const Badge = (sequelize, DataTypes) => {
  const Badge = sequelize.define('Badge', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true
    },
    minimumPosts: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  });

  return Badge;
};

module.exports = Badge;
