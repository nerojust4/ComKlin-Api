const { sequelize, Sequelize } = require('../config/database');
const UserModel = require('./User');
const RoleModel = require('./Role');
const SessionModel = require('./Session');
const AuditLogModel = require('./AuditLog');
const UserRoleModel = require('./UserRole');
const MessageModel = require('./Message');
const MessageReplyModel = require('./MessageReply');
const BlacklistedTokenModel = require('./BlacklistedToken');
const RolePermissionModel = require('./RolePermission');
const PermissionModel = require('./Permission');
const AnalyticsModel = require('./Analytics');
const PushNotificationModel = require('./PushNotification');
const AppConfigModel = require('./AppConfig');
const FeatureModel = require('./Feature');
const RoleFeatureModel = require('./RoleFeature');
const EventModel = require('./Event');
const VolunteerModel = require('./Volunteer');
const PostModel = require('./Post');
const PollutionReportModel = require('./PollutionReport');
const PostMediaModel = require('./PostMedia');
const EventMediaModel = require('./EventMedia');
const BadgeModel = require('./Badge');
const EventUserModel = require('./EventUser');
const ResourceModel = require('./Resource');

// Define models using Sequelize and associate them
const User = UserModel(sequelize, Sequelize);
const Role = RoleModel(sequelize, Sequelize);
const Session = SessionModel(sequelize, Sequelize);
const AuditLog = AuditLogModel(sequelize, Sequelize);
const UserRole = UserRoleModel(sequelize, Sequelize);
const Message = MessageModel(sequelize, Sequelize);
const MessageReply = MessageReplyModel(sequelize, Sequelize);
const BlacklistedToken = BlacklistedTokenModel(sequelize, Sequelize);
const Permission = PermissionModel(sequelize, Sequelize);
const RolePermission = RolePermissionModel(sequelize, Sequelize);
const Analytics = AnalyticsModel(sequelize, Sequelize);
const PushNotification = PushNotificationModel(sequelize, Sequelize);
const AppConfig = AppConfigModel(sequelize, Sequelize);
const Feature = FeatureModel(sequelize, Sequelize);
const RoleFeature = RoleFeatureModel(sequelize, Sequelize);
const Event = EventModel(sequelize, Sequelize);
const Volunteer = VolunteerModel(sequelize, Sequelize);
const Post = PostModel(sequelize, Sequelize);
const PostMedia = PostMediaModel(sequelize, Sequelize);
const PollutionReport = PollutionReportModel(sequelize, Sequelize);
const Badge = BadgeModel(sequelize, Sequelize);
const EventUser = EventUserModel(sequelize, Sequelize);
const EventMedia = EventMediaModel(sequelize, Sequelize);
const Resource = ResourceModel(sequelize, Sequelize);

require('./Associations')(sequelize);

// Export models and Sequelize instance
module.exports = {
  User,
  Role,
  Analytics,
  Session,
  AuditLog,
  AppConfig,
  UserRole,
  Message,
  BlacklistedToken,
  Permission,
  RolePermission,
  PushNotification,
  MessageReply,
  Feature,
  RoleFeature,
  Volunteer,
  Badge,
  Post,
  Event,
  PostMedia,
  EventUser,
  Post,
  PollutionReport,
  Resource,
  EventMedia,
  sequelize // Sequelize instance
};
