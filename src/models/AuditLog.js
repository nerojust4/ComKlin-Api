const AuditLog = (sequelize, DataTypes) => {
  return sequelize.define('AuditLog', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    action: {
      type: DataTypes.STRING,
      allowNull: false
    },
    details: {
      type: DataTypes.STRING,
      allowNull: false
    },
    client: {
      type: DataTypes.STRING,
      allowNull: true,
      defaulValue: 'None'
    },
    ipAddress: {
      type: DataTypes.STRING,
      allowNull: true
    },
    deviceId: {
      type: DataTypes.STRING,
      allowNull: true
    },
    appVersion: {
      type: DataTypes.STRING,
      allowNull: true
    },
    phoneType: {
      type: DataTypes.STRING,
      allowNull: true
    },
    model: {
      type: DataTypes.STRING,
      allowNull: true
    },
    systemName: {
      type: DataTypes.STRING,
      allowNull: true
    },
    systemVersion: {
      type: DataTypes.STRING,
      allowNull: true
    },
    bundleId: {
      type: DataTypes.STRING,
      allowNull: true
    }
  });
};
module.exports = AuditLog;
