const defineAssociations = (sequelize) => {
  const {
    User,
    Session,
    RolePermission,
    Role,
    AuditLog,
    Analytics,
    Permission,
    Feature,
    RoleFeature,
    Volunteer,
    Event,
    PostMedia,
    PollutionReport,
    Badge,
    Post,EventMedia,
    EventUser
  } = sequelize.models;

  User.belongsTo(Role, { foreignKey: 'roleId', as: 'role' });
  User.hasOne(Session, { foreignKey: 'userId', as: 'session' });
  User.hasOne(AuditLog, { foreignKey: 'userId', as: 'auditLog' });
  User.hasMany(Analytics, { foreignKey: 'userId', as: 'analytics' });
  User.hasMany(Post, { foreignKey: 'postId', as: 'posts' });
  User.hasMany(PollutionReport, { foreignKey: 'userId' });

  PollutionReport.belongsTo(User, { foreignKey: 'userId' });

  Post.hasMany(PostMedia, {
    foreignKey: 'postId',
    as: 'media'
  });

  PostMedia.belongsTo(Post, {
    foreignKey: 'postId',
    as: 'post'
  });
  
  
  Event.hasMany(EventMedia, {
    foreignKey: 'eventId',
    as: 'media'
  });

  EventMedia.belongsTo(Event, {
    foreignKey: 'eventId',
    as: 'evennt'
  });

  Post.belongsTo(User, {
    foreignKey: 'userId',
    as: 'user'
  });
  EventUser.belongsTo(User, { foreignKey: 'userId' });
  EventUser.belongsTo(Event, { foreignKey: 'eventId' });
  //Event.belongsToMany(User, { through: EventUser, foreignKey: 'eventId' });
  //User.belongsToMany(Event, { through: EventUser, foreignKey: 'userId' });

  User.belongsTo(Badge, { as: 'badge', foreignKey: 'badgeId' });

  User.hasMany(Volunteer, { foreignKey: 'userId', as: 'user' });

  Event.hasMany(Volunteer, { foreignKey: 'eventId', as: 'event' });
  Volunteer.belongsTo(User, { foreignKey: 'userId', as: 'user' });
  Volunteer.belongsTo(Event, { foreignKey: 'eventId', as: 'event' });
  // RoleFeature Model Associations
  RoleFeature.belongsTo(Feature, { foreignKey: 'featureId', as: 'feature' });
  RoleFeature.belongsTo(Role, { foreignKey: 'roleId', as: 'role' });

  // Role Associations
  Role.belongsToMany(Feature, {
    through: RoleFeature,
    foreignKey: 'roleId',
    otherKey: 'featureId',
    as: 'features'
  });

  // Feature Associations
  Feature.belongsToMany(Role, {
    through: RoleFeature,
    foreignKey: 'featureId',
    otherKey: 'roleId',
    as: 'roles'
  });

  // RolePermission Model Associations
  RolePermission.belongsTo(Role, { foreignKey: 'roleId', as: 'role' });
  RolePermission.belongsTo(Permission, {
    foreignKey: 'permissionId',
    as: 'permission'
  });
};
module.exports = defineAssociations;
