const UserRole = (sequelize, DataTypes) => {
  return sequelize.define("UserRole", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      // references: {
      //   model: "Users",
      //   key: "id",
      // },
    },
    roleId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      // references: {
      //   model: "Role",
      //   key: "id",
      // },
    },
  });
};
module.exports = UserRole;
