const BlacklistedToken = (sequelize, DataTypes) => {
  return sequelize.define("BlacklistedToken", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    token: {
      type: DataTypes.STRING,
      allowNull: false,
      // unique: true,
    },
    reason: {
      type: DataTypes.STRING,
      allowNull: true,
    },
  });
};

module.exports = BlacklistedToken;
