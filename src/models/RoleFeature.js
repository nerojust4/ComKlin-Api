const RoleFeature = (sequelize, DataTypes) => {
  const RoleFeature = sequelize.define('RoleFeature', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    }
  });

  return RoleFeature;
};

module.exports = RoleFeature;
