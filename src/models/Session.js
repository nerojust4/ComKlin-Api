const Session = (sequelize, DataTypes) => {
  return sequelize.define("Session", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    // userId: {
    //   type: DataTypes.INTEGER,
    //   allowNull: false,
    //   // references: {
    //   //   model: "Users",
    //   //   key: "id",
    //   // },
    // },
    token: {
      type: DataTypes.STRING,
      allowNull: false,
      // unique: true,
    },
    expiresAt: {
      type: DataTypes.DATE,
      allowNull: false,
    },
  });
};
module.exports = Session;
