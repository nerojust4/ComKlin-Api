const AdminUser = (sequelize, DataTypes) => {
  const AdminUser = sequelize.define("AdminUser", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    adminId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  });

  return AdminUser;
};

module.exports = AdminUser;
