// models/geolocation.js
module.exports = (sequelize, DataTypes) => {
  const Geolocation = sequelize.define('Geolocation', {
    latitude: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    longitude: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  });

  Geolocation.associate = (models) => {
    Geolocation.belongsTo(models.User, {
      foreignKey: 'userId',
      as: 'user'
    });
  };

  return Geolocation;
};
