// models/Event.js

const EventUser = (sequelize, DataTypes) => {
  const EventUser = sequelize.define('EventUser', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    }
  });

  return EventUser;
};

module.exports = EventUser;
