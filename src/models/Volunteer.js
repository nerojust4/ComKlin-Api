module.exports = (sequelize, DataTypes) => {
    const Volunteer = sequelize.define('Volunteer', {
      status: {
        type: DataTypes.STRING,
        allowNull: false
      }
    });
  
    return Volunteer;
  };
  